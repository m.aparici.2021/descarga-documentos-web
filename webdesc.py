
import sys
import urllib.request
import os


class Robot:
    def __init__(self, url):
        self.url = url
        self.filename = url.replace("/", "|")+".html"

    def retrieve(self):
        try:
            if self.filename not in os.listdir():
                print("Descargando "+self.url)
                with urllib.request.urlopen(self.url) as web_f:
                    try:
                        decode_header = web_f.headers["Content-Type"].split("=")
                        decode_method = decode_header[1]
                    except KeyError:
                        print("Unspecified encoding, default used")
                        decode_method = "utf-8"

                    read_web = web_f.read()
                with open(self.filename, "w") as local_f:
                    read_str = read_web.decode(decode_method)
                    local_f.write(read_str)
        except ValueError:
            print("URL does not exist, cant be accessed or decoding error", file=sys.stderr)
            sys.exit(1)

    def show(self):
        bool_retrieve_used = False
        if self.filename not in os.listdir():
            self.retrieve()
            bool_retrieve_used = True
        with open(self.filename, "r") as local_f:
            for line in local_f:
                print(line)
        return bool_retrieve_used

    def content(self):
        output_str = ""
        try:
            with open(self.filename, "r") as local_f:
                for line in local_f:
                    output_str += line
            return output_str
        except FileNotFoundError:
            print("No such file downloaded", file=sys.stderr)
            sys.exit(1)


class Cache:
    def __init__(self):
        self.all_urls = []

    def retrieve(self, url):
        aux_robot = Robot(url)
        if url not in self.all_urls:
            self.all_urls.append(url)
        if aux_robot.filename not in os.listdir():
            aux_robot.retrieve()

    def show(self, url):
        aux_robot = Robot(url)
        bool_downloaded = aux_robot.show()
        if bool_downloaded:
            self.all_urls.append(url)

    def show_all(self):
        print(self.all_urls)

    def content(self, url):
        aux_robot = Robot(url)
        if aux_robot.filename not in os.listdir():
            self.retrieve(url)
        returned_content = aux_robot.content()
        return returned_content


if __name__ == "__main__":
    cache = Cache()
    url1 = input("Introduce url1: ")
    cache.retrieve(url1)
    url1 = input("Introduce url1: ")
    string = cache.content(url1)
    print("==================")
    cache.show(url1)
    print("==================")
    cache.show_all()

    print(string)

    sys.exit(0)
